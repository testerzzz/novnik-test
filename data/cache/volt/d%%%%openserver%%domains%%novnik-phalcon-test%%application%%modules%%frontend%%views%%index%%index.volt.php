<h2>Secure Payment Form</h2>
<form class="form" action="">

    <input type="text" name="f_name" placeholder="First Name">
    <input type="text" name="l_name" placeholder="Last Name">
    <input type="text" name="cc_number" placeholder="Credit Card Number">
    <input type="text" name="cc_cvv" placeholder="Credit Card CVV">
    <input type="button" placeholder="GO" class="submit" value="GO">

</form>
<div class="message"></div>

<script>
    $(document).ready(function () {
        $('.form .submit').click(function () {
            $.ajax({
                url: "/api/index/add",
                type: "post",
                dataType: "json",
                data: $('.form').serialize(),

                success: function (data) {
                    if (data['status'] == false) {
                        $('.message').html(data['message']);
                    } else {
                        $('.message').html(' ');
                        window.location.replace('/ourClients');
                    }



                }
            });
            return false;
        });
    });
</script>