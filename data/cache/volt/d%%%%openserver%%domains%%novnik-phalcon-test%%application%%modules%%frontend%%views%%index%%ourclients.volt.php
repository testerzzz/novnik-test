<h2>Our clients</h2>

<table class="table display" style="border: 1px solid #000">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Credit Card Number</th>
            <th>Credit Card CVV</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) { ?>
            <tr>
                <td><?= $user->getName() ?></td>
                <td><?= $user->getLastName() ?></td>
                <td><?= $user->getCardNumber() ?></td>
                <td><?= $user->getCardCVV() ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>