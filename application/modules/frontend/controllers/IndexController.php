<?php

namespace Frontend\Controller;

use App\Service\Pagination;
use Phalcon\Mvc\Controller;
use User\Model\Categories;
use User\Model\Content;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use \Api\Model\User;

class IndexController extends Controller
{

    public function indexAction()
    {
        $users             = User::find();
        $this->view->users = $users;
        foreach ($users as $user) {
            var_dump($user->toArray());
        }
    }

    public function ourClientsAction()
    {
        $users             = User::find(['order' => 'cc_number']);
        $this->view->users = $users;
        foreach ($users as $user) {
            var_dump($user->toArray());
        }
    }

}
