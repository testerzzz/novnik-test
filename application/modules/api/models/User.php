<?php

namespace Api\Model;

use \Api\Model\ModelBase as Model;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\Uniqueness;

class User extends Model
{

    public function getSource()
    {
        return "user";
    }

    public
    /** @var string $f_name */
            $f_name,
            /** @var string $l_name */
            $l_name,
            /** @var int $cc_number */
            $cc_number,
            /** @var int $cc_cvv */
            $cc_cvv;

    public function validation()
    {
        $validator = new Validation();
        return $this->validate($validator);
    }

    public function beforeValidationOnCreate()
    {
        
    }

    public function getName()
    {
        return $this->f_name;
    }

    public function getLastName()
    {
        return $this->l_name;
    }

    public function getCardNumber()
    {
        return $this->cc_number;
    }

    public function getCardCVV()
    {
        return $this->cc_cvv;
    }

}
