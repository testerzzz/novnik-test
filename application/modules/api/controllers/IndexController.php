<?php

namespace Api\Controller;

use Phalcon\Mvc\Controller;
use Phalcony\Application;
use \Api\Form\UserForm;
use \Api\Model\User;

class IndexController extends Controller
{

    public function indexAction()
    {
        $this->view->disable = true;
        return array(
            'success' => true,
            'version' => 1.0
        );
    }

    public function addAction()
    {

        $form  = new \Api\Form\UserForm();
        $model = new User();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->bind($post, $model);
            if ($form->isValid()) {
                if ($model->save() === false) {
                    return array(
                        'status' => false,
                        'version' => 1.0,
                        'error' => 'not save',
                        'message' => $model->getMessages(),
                    );
                } else {
                    return array(
                        'status' => true,
                        'version' => 1.0,
                        'error' => '',
                    );
                }
            } else {
                $_mes = $form->getMessages();
                $_arr = 'Error:<br>';
                foreach ($_mes as $_m) {
                    $_arr .= $_m . '<br>';
                }
                return array(
                    'status' => false,
                    'version' => 1.0,
                    'error' => 'not validate',
                    'message' => $_arr,
                );
            }
        }
    }

    /**
     * @return array
     */
    public function errorAction()
    {
        $this->response->setStatusCode(404, 'Not found');
        $this->logger->debug('Error to handle: ' . $this->request->getURI());

        return array(
            'success' => false,
            'url' => $this->request->getURI(),
            'parameters' => $this->dispatcher->getParams()
        );
    }

    /**
     * @return array
     */
    public function exceptionAction()
    {
        /**
         * @var $exception \Exception
         */
        $exception = $this->dispatcher->getParam('exception');

        $message = $exception->getMessage();
        if (empty($message)) {
            $message = 'Houston we have got a problem';
        }

        if ($this->application->getEnv() == Application::ENV_PRODUCTION) {
            switch ($exception->getCode()) {
                case 500:
                    $this->logger->critical((string) $exception);
                    break;
                default:
                    $this->logger->debug((string) $exception);
                    break;
            }
        } else {
            $this->logger->debug((string) $exception);
        }

        return array(
            'success' => false,
            'message' => $message,
        );
    }

}
