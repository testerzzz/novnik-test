<?php

namespace Api\Form;

abstract class FormBase extends \Phalcon\Forms\Form
{

    public function renderDecorated($name)
    {
        if (!$this->has($name)) {
            return "form element '$name' not found<br />";
        }

        $element  = $this->get($name);
        $messages = $this->getMessagesFor($element->getName());

        $html = '';
        if (count($messages)) {
            $html .= '<div class="ui error message">';
            $html .= '<div class="header">Error</div>';
            foreach ($messages as $message) {
                $html .= '<p>' . $message . '</p>';
            }
            $html .= '</div>';
        }

        if ($element instanceof Hidden) {
            echo $element;
        } else {
            switch (true) {

                default:
                    $html .= '<div class="field">';
                    $html .= $this->makeLabel($element);
                    $html .= $element;
                    $html .= '</div>';
            }
        }

        return $html;
    }

    public function renderAll()
    {
        $html = '';
        if ($this->getElements()) {
            foreach ($this->getElements() as $element) {
                $html .= $this->renderDecorated($element->getName());
            }
        }
        return $html;
    }

    private function makeLabel($element)
    {
        if ($element->getLabel()) {
            return '<label for="' . $element->getName() . '">' . $this->helper->translate($element->getLabel()) . '</label>';
        } else {
            return '';
        }
    }

}
