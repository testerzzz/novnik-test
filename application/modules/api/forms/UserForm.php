<?php

namespace Api\Form;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Forms\Element\Text;

//use Phalcon\Validation\Validator\CreditCard as CreditCardValidator; 

class UserForm extends \Api\Form\FormBase
{

    public function initialize($model = null, $option = null)
    {
        $name = new Text('f_name');
        $name->setLabel('First Name');
        $name->addValidator(
                new StringLength(
                [
            'max' => 32,
            'messageMaximum' => 'The first name is too long',
                ]
                )
        );
        $name->addValidator(
                new PresenceOf(
                [
            'message' => 'The First name is required',
                ]
                )
        );
        $this->add($name);

        $name = new Text('l_name');
        $name->setLabel('Last Name');
        $name->addValidator(
                new StringLength(
                [
            'max' => 32,
            'messageMaximum' => 'The Last name is too long',
                ]
                )
        );
        $name->addValidator(
                new PresenceOf(
                [
            'message' => 'The Last name is required',
                ]
                )
        );
        $this->add($name);

        $name = new Text('cc_number');
        $name->setLabel('Credit Card Number');
        $name->addValidator(
                new PresenceOf(
                [
            'message' => 'The number is required',
                ]
                )
        );

        $name->addValidator(new StringLength(
                [
            'max' => 10,
            'messageMaximum' => 'The number is too long',
                ]
        ));
        $name->addValidator(
                new Numericality(
                [
            'message' => 'The number is not numeric',
                ]
                )
        );

        $this->add($name);

        $name = new Text('cc_cvv', array('required' => true));
        $name->setLabel('Credit Card CVV');
        $name->addValidator(
                new Numericality(
                [
            'message' => 'The CVV is not numeric',
                ]
                )
        );
        $name->addValidator(new StringLength(
                [
            'max' => 10,
            'messageMaximum' => 'The CVV is too long',
                ]
        ));
                $name->addValidator(
                new PresenceOf(
                [
            'message' => 'The CVV is required',
                ]
                )
        );
        $this->add($name);
    }

}
